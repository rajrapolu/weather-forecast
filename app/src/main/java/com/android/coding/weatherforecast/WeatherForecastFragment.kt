package com.android.coding.weatherforecast


import android.app.Activity.RESULT_OK
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.LoaderManager
import android.support.v4.content.CursorLoader
import android.support.v4.content.Loader
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.TextView
import com.android.coding.weatherforecast.api.RetrofitClient
import com.android.coding.weatherforecast.data.WeatherContract
import com.android.coding.weatherforecast.model.Currently
import com.android.coding.weatherforecast.model.Datum
import com.android.coding.weatherforecast.model.Forecast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.Places
import com.google.android.gms.location.places.ui.PlacePicker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class WeatherForecastFragment : Fragment(), GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LoaderManager.LoaderCallbacks<Cursor> {

    private val TAG: String = WeatherForecastFragment::class.java.simpleName
    private lateinit var mRecylerView: RecyclerView
    private lateinit var mLocationText: TextView
    private var weatherAdapter: WeatherAdapter? = null
    private val PLACE_PICKER_REQUEST: Int = 1
    private var latitude: Double = 42.3601
    private var longitude: Double = -71.0589
    var cursor: Cursor? = null
    var currentCursor: Cursor? = null
    val WEATHER_LOADER: Int = 0


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView: View = inflater!!.inflate(R.layout.fragment_weather_forecast, container, false)
        setHasOptionsMenu(true)

        mLocationText = rootView.findViewById(R.id.location_text) as TextView
        mRecylerView = rootView.findViewById(R.id.recyler_view) as RecyclerView
        val divider: RecyclerView.ItemDecoration = DividerRecyclerView(resources
                .getDrawable(R.drawable.line_divider))
        mRecylerView.addItemDecoration(divider)
        mRecylerView.adapter = weatherAdapter

        var client: GoogleApiClient = GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(activity, this)
                .build()

        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity.supportLoaderManager.initLoader(WEATHER_LOADER, null, this)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.main_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val itemId: Int = item?.itemId!!
        if (itemId == R.id.menu_location) {
            try {
                //Building an intent for the place picker
                val builder: PlacePicker.IntentBuilder = PlacePicker.IntentBuilder()
                val intent: Intent = builder.build(activity)
                startActivityForResult(intent, PLACE_PICKER_REQUEST)
            } catch(e: GooglePlayServicesRepairableException) {
                Log.e(TAG, getString(R.string.gps_not_available) + e.message)
            } catch (e: GooglePlayServicesNotAvailableException) {
                Log.e(TAG, getString(R.string.gps_not_available) + e.message)
            } catch (e: Exception) {
                Log.e(TAG, getString(R.string.place_picker_exception) + e.message)
            }
            return true
        } else if (itemId == R.id.menu_refresh) {
            updateForecast()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
            val place: Place = PlacePicker.getPlace(context, data)

            latitude = place.latLng.latitude
            longitude = place.latLng.longitude
            updateForecast()
        }
    }

    override fun onConnected(p0: Bundle?) {
        Log.i(TAG, getString(R.string.client_successful))
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.i(TAG, getString(R.string.client_suspended))
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.i(TAG, getString(R.string.client_failed))
    }

    override fun onResume() {
        super.onResume()

        queryWeatherData()
        updateForecast()
    }


    private fun queryWeatherData() {
        val projection = arrayOf(WeatherContract.WeatherEntry._ID,
                WeatherContract.WeatherEntry.COLUMN_WEATHER_TIME,
                WeatherContract.WeatherEntry.COLUMN_WEATHER_MIN,
                WeatherContract.WeatherEntry.COLUMN_WEATHER_MAX)

        cursor = context.contentResolver.
                query(WeatherContract.WeatherEntry.CONTENT_URI, projection, null, null, null)

        val currentProjection = arrayOf(WeatherContract.CurrentWeatherEntry._ID,
                WeatherContract.CurrentWeatherEntry.COLUMN_WEATHER_TIME,
                WeatherContract.CurrentWeatherEntry.COLUMN_WEATHER_TEMP,
                WeatherContract.CurrentWeatherEntry.COLUMN_WEATHER_SUMMARY)

        currentCursor = context.contentResolver.
                query(WeatherContract.CurrentWeatherEntry.CONTENT_URI, currentProjection, null, null, null)

        if (cursor != null && currentCursor != null) {
            if (cursor!!.count <= 0 || currentCursor!!.count <= 0) {
                updateForecast()
            } else {
                currentCursor!!.moveToFirst()
                val currentTemp: String = "${SimpleDateFormat("EEE MMM dd")
                        .format(Date((currentCursor!!.getLong(currentCursor!!.getColumnIndex(
                                WeatherContract.CurrentWeatherEntry.COLUMN_WEATHER_TIME))) * 1000))} " +
                        "- ${currentCursor!!.getLong(currentCursor!!.getColumnIndex(
                        WeatherContract.CurrentWeatherEntry.COLUMN_WEATHER_TEMP))} " +
                        "- ${currentCursor!!.getString(currentCursor!!.getColumnIndex(
                        WeatherContract.CurrentWeatherEntry.COLUMN_WEATHER_SUMMARY))}"
                mLocationText.text = currentTemp
                weatherAdapter = WeatherAdapter(context, cursor)
                weatherAdapter!!.notifyDataSetChanged()
                mRecylerView.adapter = weatherAdapter
            }
        }
    }

    //deleting data from the database
    private fun deleteWeatherData() {
        context.contentResolver.delete(WeatherContract.WeatherEntry.CONTENT_URI, null, null)
        context.contentResolver.delete(WeatherContract.CurrentWeatherEntry.CONTENT_URI, null, null)
    }


    //a retrofit call is done in this method
    private fun updateForecast() {

        RetrofitClient.getApiService().getForecast(latitude, longitude).enqueue(object : Callback<Forecast> {
            override fun onResponse(call: Call<Forecast>?, response: Response<Forecast>?) {
                response?.isSuccessful.let {
                    gatherWeatherData(response?.body())
                }
            }

            override fun onFailure(call: Call<Forecast>?, t: Throwable?) {
                Log.e(TAG, "Network call failed: " + t)
            }
        })
    }

    //the response from api is gathered in a list and an adapter is set to the recycler view
    private fun gatherWeatherData(forecast: Forecast?) {

        deleteWeatherData()
        val insertTodatabase: InsertToDatabaseTask = InsertToDatabaseTask(context, forecast)
        insertTodatabase.execute()
    }


    override fun onPause() {
        super.onPause()
        currentCursor?.close()
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        val projection = arrayOf(WeatherContract.WeatherEntry._ID,
                WeatherContract.WeatherEntry.COLUMN_WEATHER_TIME,
                WeatherContract.WeatherEntry.COLUMN_WEATHER_MIN,
                WeatherContract.WeatherEntry.COLUMN_WEATHER_MAX)

        return CursorLoader(context, WeatherContract.WeatherEntry.CONTENT_URI,
                projection, null, null, null)
    }

    override fun onLoaderReset(loader: Loader<Cursor>?) {
        weatherAdapter?.swapCursor(null)    }

    override fun onLoadFinished(loader: Loader<Cursor>?, data: Cursor?) {
        weatherAdapter?.swapCursor(data)
    }


    //Asynctask to upload the data in to the data base on a background thread
    inner class InsertToDatabaseTask(var mContext: Context, val forecast: Forecast?) : AsyncTask<String, Void, Currently>() {
        private var forecastList: MutableList<ContentValues> = ArrayList<ContentValues>()

        override fun doInBackground(vararg params: String): Currently? {
            insertWeatherData(forecast)
            val currentTemp: Currently = insertCurrentWeather(forecast)!!
            return currentTemp
        }

        private fun insertWeatherData(forecast: Forecast?) {
            val dailyForecast: List<Datum> = forecast?.daily?.data!!
            var index: Int = 1
            var inserted: Int = 0
            while (index < dailyForecast.size) {
                var contentValues: ContentValues = ContentValues()
                contentValues.put(WeatherContract.WeatherEntry.
                        COLUMN_WEATHER_TIME, dailyForecast[index].time)
                contentValues.put(WeatherContract.WeatherEntry.
                        COLUMN_WEATHER_MIN, dailyForecast[index].temperatureMin)
                contentValues.put(WeatherContract.WeatherEntry.
                        COLUMN_WEATHER_MAX, dailyForecast[index].temperatureMax)
                forecastList.add(contentValues)
                index++
            }

            inserted = mContext.contentResolver.bulkInsert(WeatherContract.WeatherEntry.CONTENT_URI,
                    forecastList.toTypedArray())
        }

        private fun insertCurrentWeather(forecast: Forecast?): Currently? {
            val currently: Currently = forecast?.currently!!
            val contentValues: ContentValues = ContentValues()
            contentValues.put(WeatherContract.CurrentWeatherEntry.COLUMN_WEATHER_TIME,
                    currently.time)
            contentValues.put(WeatherContract.CurrentWeatherEntry.COLUMN_WEATHER_TEMP,
                    currently.temperature)
            contentValues.put(WeatherContract.CurrentWeatherEntry.COLUMN_WEATHER_SUMMARY,
                    currently.summary)

            val uri: Uri? = mContext.contentResolver.insert(
                    WeatherContract.CurrentWeatherEntry.CONTENT_URI, contentValues)

            if (uri != null) {
                return currently;

            }
            return null
        }

        override fun onPostExecute(currently: Currently?) {
            super.onPostExecute(currently)
            weatherAdapter?.notifyDataSetChanged()
            if (currently != null) {
                val currentTemp: String = "${SimpleDateFormat("EEE MMM dd")
                        .format(Date(currently.time.toLong() * 1000))} " +
                        "- ${currently.temperature} " +
                        "- ${currently.summary}"
                mLocationText.text = currentTemp
            }

        }

    }
}
