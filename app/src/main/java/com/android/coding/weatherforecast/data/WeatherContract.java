package com.android.coding.weatherforecast.data;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.provider.BaseColumns;

import com.fasterxml.jackson.databind.deser.Deserializers;

import java.util.List;

public class WeatherContract {

    public static final String CONTENT_AUTHORITY = "com.android.coding.weatherforecast.data";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_WEATHER = "weather";
    public static final String PATH_WEATHER_CURRENT = "weatherCurrent";

    public static final class WeatherEntry implements BaseColumns {

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_WEATHER);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_WEATHER;


        public static final String TABLE_NAME = "weather";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_WEATHER_TIME = "time";
        public static final String COLUMN_WEATHER_MIN = "weatherMin";
        public static final String COLUMN_WEATHER_MAX = "weatherMax";
    }

    public static final class CurrentWeatherEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                Uri.withAppendedPath(BASE_CONTENT_URI, PATH_WEATHER_CURRENT);

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" +
                        PATH_WEATHER_CURRENT;

        public static final String TABLE_NAME = "weatherCurrent";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_WEATHER_TIME = "time";
        public static final String COLUMN_WEATHER_TEMP = "currentWeather";
        public static final String COLUMN_WEATHER_SUMMARY = "currentSummary";

    }


}
