package com.android.coding.weatherforecast.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

public class WeatherProvider extends ContentProvider {

    WeatherDBHelper weatherDBHelper;
    public static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    public static final int WEATHER_DATA = 1;
    public static final int CURRENT_WEATHER = 2;

    static {
        sUriMatcher.addURI(WeatherContract.CONTENT_AUTHORITY, WeatherContract.PATH_WEATHER,
                WEATHER_DATA);
        sUriMatcher.addURI(WeatherContract.CONTENT_AUTHORITY, WeatherContract.PATH_WEATHER_CURRENT,
                CURRENT_WEATHER);
    }

    @Override
    public boolean onCreate() {
        weatherDBHelper = new WeatherDBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection,
                        @Nullable String selection, @Nullable String[] selectionArgs,
                        @Nullable String sortOrder) {
        SQLiteDatabase database = weatherDBHelper.getReadableDatabase();
        Cursor cursor;

        int match = sUriMatcher.match(uri);
        switch (match) {
            case WEATHER_DATA:
                cursor = database.query(WeatherContract.WeatherEntry.TABLE_NAME, projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            case CURRENT_WEATHER:
                cursor = database.query(WeatherContract.CurrentWeatherEntry.TABLE_NAME, projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Cannot query unknown uri: " + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case WEATHER_DATA:
                return WeatherContract.WeatherEntry.CONTENT_LIST_TYPE;
            case CURRENT_WEATHER:
                return WeatherContract.CurrentWeatherEntry.CONTENT_LIST_TYPE;
            default:
                throw new IllegalStateException("Unknown uri " + uri + "with match " + match);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case WEATHER_DATA:
                return insertWeatherData(uri, values);
            case CURRENT_WEATHER:
                return insertCurrentWeather(uri, values);
            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);
        }
    }

    private Uri insertCurrentWeather(Uri uri, ContentValues values) {
        SQLiteDatabase database = weatherDBHelper.getWritableDatabase();
        long rowId = database.insert(WeatherContract.CurrentWeatherEntry.TABLE_NAME, null, values);
        if (rowId == -1) {
            Toast.makeText(getContext(), "Failed to insert item", Toast.LENGTH_SHORT).show();
            return null;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return ContentUris.withAppendedId(uri, rowId);
    }

    private Uri insertWeatherData(Uri uri, ContentValues values) {
        SQLiteDatabase database = weatherDBHelper.getWritableDatabase();
        long rowId = database.insert(WeatherContract.WeatherEntry.TABLE_NAME, null, values);
        if (rowId == -1) {
            Toast.makeText(getContext(), "Failed to insert item", Toast.LENGTH_SHORT).show();
            return null;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return ContentUris.withAppendedId(uri, rowId);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection,
                      @Nullable String[] selectionArgs) {
        SQLiteDatabase database = weatherDBHelper.getWritableDatabase();
        int rowsDeleted;

        int match = sUriMatcher.match(uri);
        switch (match) {
            case WEATHER_DATA:
                rowsDeleted = database.delete(WeatherContract.WeatherEntry.TABLE_NAME,
                        selection, selectionArgs);
                break;
            case CURRENT_WEATHER:
                rowsDeleted = database.delete(WeatherContract.CurrentWeatherEntry.TABLE_NAME,
                        selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Deletion is supported " + uri);
        }
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values,
                      @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        SQLiteDatabase database = weatherDBHelper.getWritableDatabase();
        int match = sUriMatcher.match(uri);

        switch (match) {
            case WEATHER_DATA:
                database.beginTransaction();
                int count = 0;
                try {
                    for (ContentValues value: values) {
                        long _id = database.insert(WeatherContract.WeatherEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            count++;
                        }
                    }
                    database.setTransactionSuccessful();
                } finally {
                    database.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return count;
            default:
                return super.bulkInsert(uri, values);
        }
    }
}
