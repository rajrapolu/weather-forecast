package com.android.coding.weatherforecast.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.android.coding.weatherforecast.data.WeatherContract.WeatherEntry;
import com.android.coding.weatherforecast.data.WeatherContract.CurrentWeatherEntry;

public class WeatherDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "weather.db";
    private static final int DATABASE_VERSION = 1;

    public WeatherDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_WEATHER_STATEMENT =
                "CREATE TABLE " + WeatherEntry.TABLE_NAME + "(" + WeatherEntry._ID
                        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + WeatherEntry.COLUMN_WEATHER_TIME + " LONG NOT NULL, "
                        + WeatherEntry.COLUMN_WEATHER_MIN + " DOUBLE NOT NULL, "
                        + WeatherEntry.COLUMN_WEATHER_MAX + " DOUBLE NOT NULL);";

        final String SQL_CURRENT_STATEMENT =
                "CREATE TABLE " + CurrentWeatherEntry.TABLE_NAME + "(" + CurrentWeatherEntry._ID
                        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + CurrentWeatherEntry.COLUMN_WEATHER_TIME + " LONG NOT NULL, "
                        + CurrentWeatherEntry.COLUMN_WEATHER_TEMP + " DOUBLE NOT NULL, "
                        + CurrentWeatherEntry.COLUMN_WEATHER_SUMMARY + " STRING NOT NULL);";

        db.execSQL(SQL_WEATHER_STATEMENT);
        db.execSQL(SQL_CURRENT_STATEMENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
