package com.android.coding.weatherforecast.api;

import com.android.coding.weatherforecast.model.Forecast;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {

    @GET("{latitude},{longitude}?exclude=[minutely,hourly,alerts,flags]")
    Call<Forecast> getForecast(@Path("latitude") Double latitude,
                               @Path("longitude") Double longitude);
}
