package com.android.coding.weatherforecast.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static ApiService apiService;
    private static String baseURL =
            "https://api.darksky.net/forecast/203bf0976335ed98863b556ed9f61f79/";

    public static ApiService getApiService() {
        if (apiService == null) {
            apiService = new Retrofit.Builder()
                    .baseUrl(baseURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(ApiService.class);
            return apiService;
        }
        return apiService;
    }
}
