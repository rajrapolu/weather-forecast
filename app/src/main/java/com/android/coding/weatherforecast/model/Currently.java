package com.android.coding.weatherforecast.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "time",
        "summary",
        "icon",
        "nearestStormDistance",
        "nearestStormBearing",
        "precipIntensity",
        "precipProbability",
        "temperature",
        "apparentTemperature",
        "dewPoint",
        "humidity",
        "windSpeed",
        "windBearing",
        "visibility",
        "cloudCover",
        "pressure",
        "ozone"
})
public class Currently {

    @JsonProperty("time")
    private int time;
    @JsonProperty("summary")
    private String summary;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("nearestStormDistance")
    private int nearestStormDistance;
    @JsonProperty("nearestStormBearing")
    private int nearestStormBearing;
    @JsonProperty("precipIntensity")
    private Double precipIntensity;
    @JsonProperty("precipProbability")
    private Double precipProbability;
    @JsonProperty("temperature")
    private double temperature;
    @JsonProperty("apparentTemperature")
    private double apparentTemperature;
    @JsonProperty("dewPoint")
    private double dewPoint;
    @JsonProperty("humidity")
    private double humidity;
    @JsonProperty("windSpeed")
    private double windSpeed;
    @JsonProperty("windBearing")
    private int windBearing;
    @JsonProperty("visibility")
    private double visibility;
    @JsonProperty("cloudCover")
    private double cloudCover;
    @JsonProperty("pressure")
    private double pressure;
    @JsonProperty("ozone")
    private double ozone;

    @JsonProperty("time")
    public int getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(int time) {
        this.time = time;
    }

    @JsonProperty("summary")
    public String getSummary() {
        return summary;
    }

    @JsonProperty("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }

    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    @JsonProperty("icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }

    @JsonProperty("nearestStormDistance")
    public int getNearestStormDistance() {
        return nearestStormDistance;
    }

    @JsonProperty("nearestStormDistance")
    public void setNearestStormDistance(int nearestStormDistance) {
        this.nearestStormDistance = nearestStormDistance;
    }

    @JsonProperty("nearestStormBearing")
    public int getNearestStormBearing() {
        return nearestStormBearing;
    }

    @JsonProperty("nearestStormBearing")
    public void setNearestStormBearing(int nearestStormBearing) {
        this.nearestStormBearing = nearestStormBearing;
    }

    @JsonProperty("precipIntensity")
    public Double getPrecipIntensity() {
        return precipIntensity;
    }

    @JsonProperty("precipIntensity")
    public void setPrecipIntensity(Double precipIntensity) {
        this.precipIntensity = precipIntensity;
    }

    @JsonProperty("precipProbability")
    public Double getPrecipProbability() {
        return precipProbability;
    }

    @JsonProperty("precipProbability")
    public void setPrecipProbability(Double precipProbability) {
        this.precipProbability = precipProbability;
    }

    @JsonProperty("temperature")
    public double getTemperature() {
        return temperature;
    }

    @JsonProperty("temperature")
    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    @JsonProperty("apparentTemperature")
    public double getApparentTemperature() {
        return apparentTemperature;
    }

    @JsonProperty("apparentTemperature")
    public void setApparentTemperature(double apparentTemperature) {
        this.apparentTemperature = apparentTemperature;
    }

    @JsonProperty("dewPoint")
    public double getDewPoint() {
        return dewPoint;
    }

    @JsonProperty("dewPoint")
    public void setDewPoint(double dewPoint) {
        this.dewPoint = dewPoint;
    }

    @JsonProperty("humidity")
    public double getHumidity() {
        return humidity;
    }

    @JsonProperty("humidity")
    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    @JsonProperty("windSpeed")
    public double getWindSpeed() {
        return windSpeed;
    }

    @JsonProperty("windSpeed")
    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    @JsonProperty("windBearing")
    public int getWindBearing() {
        return windBearing;
    }

    @JsonProperty("windBearing")
    public void setWindBearing(int windBearing) {
        this.windBearing = windBearing;
    }

    @JsonProperty("visibility")
    public double getVisibility() {
        return visibility;
    }

    @JsonProperty("visibility")
    public void setVisibility(double visibility) {
        this.visibility = visibility;
    }

    @JsonProperty("cloudCover")
    public double getCloudCover() {
        return cloudCover;
    }

    @JsonProperty("cloudCover")
    public void setCloudCover(double cloudCover) {
        this.cloudCover = cloudCover;
    }

    @JsonProperty("pressure")
    public double getPressure() {
        return pressure;
    }

    @JsonProperty("pressure")
    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    @JsonProperty("ozone")
    public double getOzone() {
        return ozone;
    }

    @JsonProperty("ozone")
    public void setOzone(double ozone) {
        this.ozone = ozone;
    }

}
