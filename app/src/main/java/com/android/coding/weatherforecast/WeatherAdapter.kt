package com.android.coding.weatherforecast

import android.content.Context
import android.database.Cursor
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.android.coding.weatherforecast.data.WeatherContract
import java.text.SimpleDateFormat
import java.util.*

class WeatherAdapter(var context: Context, cursorData: Cursor?):
        RecyclerView.Adapter<WeatherAdapter.ViewHolder>() {

    var cursor: Cursor? = cursorData

    override fun getItemCount(): Int {
        if (cursor != null) {
            return cursor!!.count
        } else {
            return 0
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        return ViewHolder(inflater.inflate(R.layout.list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        cursor?.moveToPosition(position)
        holder?.dayForecast!!.text = SimpleDateFormat("EEE MMM dd")
                .format(Date((cursor!!.getLong(cursor!!.getColumnIndex(
                        WeatherContract.WeatherEntry.COLUMN_WEATHER_TIME))) * 1000))
        holder.forecastMax.text = cursor!!.getString(
                cursor!!.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_WEATHER_MAX))
        holder.forecastMin.text = cursor!!.getString(
                cursor!!.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_WEATHER_MIN))
    }

    fun swapCursor(changeCursor: Cursor?): Unit {
        cursor = changeCursor
        if (cursor != null) {
            this.notifyDataSetChanged()
        }
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var dayForecast: TextView
        var forecastMin: TextView
        var forecastMax: TextView

        init {
            itemView.tag = this
            dayForecast = itemView.findViewById(R.id.recycler_view_text_view) as TextView
            forecastMax = itemView.findViewById(R.id.text_max) as TextView
            forecastMin = itemView.findViewById(R.id.text_min) as TextView
        }
    }

}