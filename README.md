The project helps in displaying the weather forecast to the user based on the location they have selected.

The application displays the current location at latitude and longitude of 42.3601, and -71.0589 respectively. Once, the user has selected a location from the action bar, the current forecast, and the forecast for the next 6 days will be displayed. 
 
Retrofit is used for network calls, Kotlin is used as the primary language in conjunction with Java. Recycler view is used to display a list of data. Google Place Service API, Place picker is used to provide the option of picking a place for the user.

The application also uses SQLite database to persistently store the data at the client. Asynctask and also cursor loaders were used to perform operations on a background thread.